/*
 * bluetooth_uart.c
 *
 *  Created on: Dec 8, 2017
 *      Author: KiranHegde
 */


#include "bluetooth_uart.h"
#include "vs1053_decoder.h"
#include <stdio.h>

uint8_t uart_temp=0;

void UART_configure()
{
     CS->KEY = CS_KEY_VAL;                   // Unlock CS module for register access
    CS->CTL0 = 0;                           // Reset tuning parameters
    CS->CTL0 = CS_CTL0_DCORSEL_3;           // Set DCO to 12MHz (nominal, center of 8-16MHz range)
    CS->CTL1 = CS_CTL1_SELA_2 |             // Select ACLK = REFO
            CS_CTL1_SELS_3 |                // SMCLK = DCO
            CS_CTL1_SELM_3;                 // MCLK = DCO
    CS->KEY = 0;

    P1->SEL0 |= BIT2 | BIT3;
        // Configure UART
        EUSCI_A0->CTLW0 |= EUSCI_A_CTLW0_SWRST; // Put eUSCI in reset
        EUSCI_A0->CTLW0 = EUSCI_A_CTLW0_SWRST | // Remain eUSCI in reset
                EUSCI_B_CTLW0_SSEL__SMCLK;      // Configure eUSCI clock source for SMCLK
        // Baud Rate calculation
        // 32000000/(16*9600) = 208.125
        EUSCI_A0->BRW = 78;                     // 32000000/(16*9600)
        EUSCI_A0->MCTLW = (2 << EUSCI_A_MCTLW_BRF_OFS) |
                EUSCI_A_MCTLW_OS16;

        EUSCI_A0->CTLW0 &= ~EUSCI_A_CTLW0_SWRST; // Initialize eUSCI
        EUSCI_A0->IFG &= ~EUSCI_A_IFG_RXIFG;    // Clear eUSCI RX interrupt flag
        EUSCI_A0->IE |= EUSCI_A_IE_RXIE;        // Enable USCI_A0 RX interrupt

        // Enable global interrupt
        __enable_irq();

        // Enable eUSCIA0 interrupt in NVIC module
        NVIC->ISER[0] = 1 << ((EUSCIA0_IRQn) & 31);
}


void EUSCIA0_IRQHandler(void)
{
    __disable_irq();
    if (EUSCI_A0->IFG & EUSCI_A_IFG_RXIFG)
    {
        uart_temp = EUSCI_A0->RXBUF;
        switch(uart_temp)
        {
            case ('5'): status=playing;
                        prev_status=playing;
                         break;
            case ('A'): status=paused;
                        prev_status=paused;
                         break;
            case ('1'): status=stop;
                         break;
            case ('2'): status=volumeup;
                         lower_status = volume;
                         break;
            case ('3'): status=volumedown;
                         lower_status = volume;
                         break;
            case ('4'): status=nextsong;
                         lower_status=speed;
                         break;
            case ('6'): status=prevsong;
                         lower_status=speed;
                         break;
            case ('7'): status=mute;
                        lower_status=volume;
                        break;
            case ('B'): status=unmute;
                        lower_status=volume;
                        break;
            case ('8'): status=fastplay;
                         lower_status=speed;
                         break;
            case ('9'): status=normalplay;
                         lower_status=speed;
                         break;
            default: ;  break;
        }
    }
    __enable_irq();
}

