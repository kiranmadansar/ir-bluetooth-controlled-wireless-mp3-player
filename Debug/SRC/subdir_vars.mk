################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../SRC/ff.c \
../SRC/ffsystem.c \
../SRC/ffunicode.c \
../SRC/main.c \
../SRC/mmc-msp432p401r.c \
../SRC/spi_driver.c 

C_DEPS += \
./SRC/ff.d \
./SRC/ffsystem.d \
./SRC/ffunicode.d \
./SRC/main.d \
./SRC/mmc-msp432p401r.d \
./SRC/spi_driver.d 

OBJS += \
./SRC/ff.obj \
./SRC/ffsystem.obj \
./SRC/ffunicode.obj \
./SRC/main.obj \
./SRC/mmc-msp432p401r.obj \
./SRC/spi_driver.obj 

OBJS__QUOTED += \
"SRC\ff.obj" \
"SRC\ffsystem.obj" \
"SRC\ffunicode.obj" \
"SRC\main.obj" \
"SRC\mmc-msp432p401r.obj" \
"SRC\spi_driver.obj" 

C_DEPS__QUOTED += \
"SRC\ff.d" \
"SRC\ffsystem.d" \
"SRC\ffunicode.d" \
"SRC\main.d" \
"SRC\mmc-msp432p401r.d" \
"SRC\spi_driver.d" 

C_SRCS__QUOTED += \
"../SRC/ff.c" \
"../SRC/ffsystem.c" \
"../SRC/ffunicode.c" \
"../SRC/main.c" \
"../SRC/mmc-msp432p401r.c" \
"../SRC/spi_driver.c" 


