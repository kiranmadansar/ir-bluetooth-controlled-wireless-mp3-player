################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432p401r.cmd 

LIB_SRCS += \
../msp432p4xx_driverlib.lib 

C_SRCS += \
../bluetooth_uart.c \
../startup_msp432p401r_ccs.c \
../system_msp432p401r.c \
../vs1053_decoder.c 

C_DEPS += \
./bluetooth_uart.d \
./startup_msp432p401r_ccs.d \
./system_msp432p401r.d \
./vs1053_decoder.d 

OBJS += \
./bluetooth_uart.obj \
./startup_msp432p401r_ccs.obj \
./system_msp432p401r.obj \
./vs1053_decoder.obj 

OBJS__QUOTED += \
"bluetooth_uart.obj" \
"startup_msp432p401r_ccs.obj" \
"system_msp432p401r.obj" \
"vs1053_decoder.obj" 

C_DEPS__QUOTED += \
"bluetooth_uart.d" \
"startup_msp432p401r_ccs.d" \
"system_msp432p401r.d" \
"vs1053_decoder.d" 

C_SRCS__QUOTED += \
"../bluetooth_uart.c" \
"../startup_msp432p401r_ccs.c" \
"../system_msp432p401r.c" \
"../vs1053_decoder.c" 


