/*
 * spi_driver.h
 *
 *  Created on: Dec 2, 2017
 *      Author: KiranHegde
 */

#ifndef SPI_DRIVER_H_
#define SPI_DRIVER_H_

#include "msp.h"
#include <stdio.h>
#include "ff.h"
#include "ffconf.h"
#include "integer.h"

void SDCS_configure();
void SPI_initialize();
BYTE SPI_read();
void SPI_write(BYTE);

#endif /* SPI_DRIVER_H_ */
