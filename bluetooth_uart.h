/*
 * uart.h
 *
 *  Created on: Dec 8, 2017
 *      Author: KiranHegde
 */

#ifndef BLUETOOTH_UART_H_
#define BLUETOOTH_UART_H_

#include "msp.h"

extern uint8_t status;
extern uint8_t lower_status;

void UART_configure();



#endif /* BLUETOOTH_UART_H_ */
