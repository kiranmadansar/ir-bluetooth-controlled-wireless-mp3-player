/*
 * vs1063_decoder.c
 *
 *  Created on: Dec 4, 2017
 *      Author: KiranHegde
 */

#include "vs1053_decoder.h"
#include "spi_driver.h"
#include <stdbool.h>

uint16_t vol_beforemute;

#define VS_DREQ (P2->IN&BIT7)


void vs_gpioConfigure()
{
    P2->DIR |= BIT4 | BIT6; // BIT4=DCS, BIT6=CS,
    P2->OUT |= BIT4 | BIT6;
    P5->DIR |= BIT6;                //BIT6 = RST
    P5->OUT |= BIT6;
    P2->DIR &= ~BIT7; // BIT7=DREQ
    P2->REN |= BIT7;
}



void vs_hardReset(void)
{
    int t=0;
    P5OUT &= ~BIT6;
    for(t=0; t<100; t++)
    {
        delay();
    }
    P5OUT |= BIT6;
}

void vs_softReset(void)
{
   vs_sciWrite(SCI_MODE, 0x0004);
}

void delay()
{
    __delay_cycles(10000);
}

void vs_sciWrite(uint8_t reg, uint16_t value)
{
    while(vs_readyData())
        {
            delay();
        }
    ASSERT_VSCS();
    SPI_write(VS_WRITE_COMMAND);
    SPI_write(reg);
    SPI_write((uint8_t)(value>>8)); //high byte
    SPI_write((uint8_t)(value & 0xFF));    // low byte
    while(vs_readyData())
        {
            delay();
        }
    DEASSERT_VSCS();
}

uint16_t vs_sciRead(uint8_t reg)
{
    uint8_t low, high;
    while(vs_readyData())
            {
                delay();
            }
        ASSERT_VSCS();
        SPI_write(VS_READ_COMMAND);
        SPI_write(reg);
        high = SPI_read(); //high byte
        low = SPI_read();    // low byte
        while(vs_readyData())
            {
                delay();
            }
        DEASSERT_VSCS();
        return ((uint16_t)(high<<8))+low;
}

void vs_sdiWrite(uint8_t *ptr, uint32_t len)
{
    int i=0, tmp=0;
    if(ptr)
    {

        while(i<(sizeof(line)/32))
        {
            while(vs_readyData())
            {
                delay();
            }
            ASSERT_VSDCS();
            while(tmp<32)
            {
                SPI_write(*ptr++);
                tmp++;
            }
            DEASSERT_VSDCS();
            tmp=0;
            i++;
        }

    }
}

uint16_t vs_decodeTime(void)
{
    return (uint16_t)vs_sciRead(SCI_DECODE_TIME);
}

void vs_setVolume(uint8_t left, uint8_t right)
{
    uint16_t temp =((uint16_t)(left<<8)+right);
    if(temp>0x0000 && temp<=0xFEFE)
    {
        vs_sciWrite(SCI_VOL, temp);
    }
}

bool vs_readyData(void)
{
    if(!VS_DREQ)
        return true;
    else
        return false;
}

void vs_Initialization()
{
    vs_hardReset();
    vs_sciWrite(SCI_CLOCKF, 0x9be8);
    delay();
    delay();
    while(vs_readyData())
    {
        delay();
    }
    vs_setVolume(0x7F, 0x7F);
    vs_sciWrite(SCI_MODE, 0x0880);
    vs_sciWrite(SCI_AUDATA, 0xAC45);
    delay();
    delay();
}

void vs_sineTestStart()
{
        uint16_t temp = vs_sciRead(SCI_MODE);
        vs_sciWrite(SCI_MODE, temp | SM_TESTS);
        ASSERT_VSDCS();
        while(vs_readyData())
        {
            delay();
        }
        SPI_write(0x53);
        SPI_write(0xEF);
        SPI_write(0x6E);
        SPI_write(126);
        SPI_write(0);
        SPI_write(0);
        SPI_write(0);
        SPI_write(0);
        while(vs_readyData())
        {
            delay();
        }
        DEASSERT_VSDCS();
}

void vs_sineTestStop()
{
        uint16_t temp = vs_sciRead(SCI_MODE);
        vs_sciWrite(SCI_MODE, temp | SM_TESTS);
        ASSERT_VSDCS();
        while(vs_readyData())
        {
            delay();
        }
        SPI_write(0x45);
        SPI_write(0x78);
        SPI_write(0x69);
        SPI_write(0x74);
        SPI_write(0);
        SPI_write(0);
        SPI_write(0);
        SPI_write(0);
        while(vs_readyData())
        {
            delay();
        }
        vs_sciWrite(SCI_MODE, temp);
}


void vs_sendZeros()
{
    int var;
    ASSERT_VSDCS();

    for (var=0; var<=256; var++)
    {
        while(vs_readyData())
        {
             delay();
        }
        SPI_write(0x00);
    }
    while(vs_readyData())
    {
         delay();
    }
    DEASSERT_VSDCS();
}


void vs_VolumeUp()
{
    if(vol_var<=14)
    {
        vol_var++;
        uint16_t volume_temp;
        if(mute_status!=mute)
        {
            volume_temp = vs_sciRead(SCI_VOL);
            volume_temp -=0x0F0F;
            if(volume_temp >= 0)
                vs_sciWrite(SCI_VOL, (uint16_t)volume_temp);
        }
        else
        {
            vol_beforemute -= 0x0F0F;
            vs_sciWrite(SCI_VOL, (uint16_t)vol_beforemute);
            mute_status=unmute;
        }
    }
}

void vs_VolumeDown()
{
    if(vol_var>=1)
    {
        vol_var--;
        uint16_t volume_temp;
        volume_temp = vs_sciRead(SCI_VOL);
        volume_temp += 0x0F0F;
        if(volume_temp <= 0xFEFE)
            vs_sciWrite(SCI_VOL, (uint16_t)volume_temp);

    }
}

void vs_VolumeMute()
{
    vol_beforemute = vs_sciRead(SCI_VOL);
    uint16_t volume_temp=0xFEFE;
    vs_sciWrite(SCI_VOL, (uint16_t)volume_temp);
    mute_status=mute;
}

void vs_VolumeUnmute()
{
    vs_sciWrite(SCI_VOL, (uint16_t)vol_beforemute);
    mute_status=unmute;
}

void vs_FastPlay()
{
        vs_sciWrite(SCI_WRAMADDR, 0x1E04);
        vs_sciWrite(SCI_WRAM, 0x0002);
}

void vs_NormalPlay()
{
    vs_sciWrite(SCI_WRAMADDR, 0x1E04);
    vs_sciWrite(SCI_WRAM, 0x0001);
}
