#include "spi_driver.h"
#include "driverlib.h"

#define ASSERT_SDCS() (P5OUT &= ~BIT0)
#define DEASSERT_SDCS() (P5OUT |= BIT0)

const eUSCI_SPI_MasterConfig spiMasterConfig =
{
        EUSCI_B_SPI_CLOCKSOURCE_SMCLK,             // SMCLK Clock Source
        3000000,                                   // SMCLK 3Mhz
        200000,                                    // SPICLK = 200khz
        EUSCI_B_SPI_MSB_FIRST,                     // MSB First
        EUSCI_B_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT,    // Phase
        EUSCI_B_SPI_CLOCKPOLARITY_INACTIVITY_HIGH, // High polarity
        EUSCI_B_SPI_3PIN                           // 3Wire SPI Mode
};

void CS_configure()
{
    P5->DIR |= BIT0 | BIT2;
    P5->OUT |= BIT0 | BIT2;
}



void SPI_initialize()
{
    P1->SEL0 |= BIT5 | BIT6 | BIT7;         // Set P1.5, P1.6, and P1.7 as
                                                // SPI pins functionality
    CS_configure();

    SPI_initMaster(EUSCI_B0_BASE, &spiMasterConfig);

        /* Enable SPI module */
    SPI_enableModule(EUSCI_B0_BASE);
    P1REN |= BIT7;
    SPI_write(0xFF);

}

void SPI_write(BYTE data_val)
{
    //ASSERT_SDCS();
    {
        while(!(UCB0IFG&UCTXIFG));
        UCB0TXBUF = data_val;
        while(!(UCB0IFG&UCRXIFG));
        UCB0RXBUF;
    }
    //DEASSERT_SDCS();
}

BYTE SPI_read()
{
    BYTE temp;
    //ASSERT_SDCS();
    {
        while (!(UCB0IFG&UCTXIFG));
        UCB0TXBUF = 0xFF;
        while (!(UCB0IFG&UCRXIFG));
        temp = UCB0RXBUF;
    }
    //DEASSERT_SDCS();
    return temp;
}


