#include "msp.h"
#include <stdio.h>
#include <stdlib.h>
#include "spi_driver.h"
#include "ff.h"
#include "ffconf.h"
#include "integer.h"
#include "driverlib.h"
#include "vs1053_decoder.h"
#include "bluetooth_uart.h"

char line[4096];

uint8_t status;
uint8_t lower_status;
uint8_t mute_status;
uint8_t vol_var;
uint8_t prev_status;

void SysTick_Handler(void)
{
    disk_timerproc();
}

void clock_setup()
{
    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_24);
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    CS_initClockSignal(CS_HSMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
}

void SysTick_configure()
{
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1,
    GPIO_PIN0, GPIO_PRIMARY_MODULE_FUNCTION);
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);
    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
    SysTick_setPeriod(32000000 / 100);
    SysTick_enableModule();
    SysTick_enableInterrupt();
}

void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer
    FRESULT ff, fk;
    vol_var=7;
    prev_status=playing;
    uint8_t song_number=0;
    status=stopped;
    lower_status=nothing;
    mute_status=unmute;
    clock_setup();
    FIL fil;
    static FATFS *g_sFatFs;
    UINT br;
    g_sFatFs = (FATFS *) malloc(sizeof (FATFS));
    UART_configure();
	SysTick_configure();
    SPI_initialize();
    printf("Started\n\r");
    vs_gpioConfigure();
    vs_hardReset();
    vs_Initialization();
	int kkk=5;
	do
	{
	    fk = f_mount(g_sFatFs,"", 1);
	    kkk--;
	}while(fk!=FR_OK && kkk>=0);

    if(fk!=FR_OK)
    {
        printf("f_mount error: %d\n", fk);
    }
    else
    {
        while(1)
        {

            if(status!=stopped)
            {
                switch(song_number)
                {
                case (0): ff = f_open(&fil, "track01.mp3", FA_READ);
                                break;
                case (1): ff = f_open(&fil, "track02.mp3", FA_READ);
                                break;
                case (2): ff = f_open(&fil, "track03.mp3", FA_READ);
                                break;
                case (3): ff = f_open(&fil, "track04.mp3", FA_READ);
                                break;
                case (4): ff = f_open(&fil, "track05.mp3", FA_READ);
                                break;
                default: ff = 100;
                }
                if(ff!=FR_OK)
                {
                    printf("f_open error: %d\n", ff);
                }
                else
                {
                    do
                    {
                        while(status==paused);
                        if(status!=playing)
                        {
                            /*if(status==pause)
                            {
                                status=paused;
                            }*/
                            if(status==stop)
                            {
                                status=stopped;
                                prev_status=playing;
                                break;
                            }
                            if(lower_status==volume || lower_status==speed)
                            {
                                if(status==nextsong)
                                {
                                    status=prev_status;;
                                    if(song_number<4)
                                        song_number++;
                                    else
                                        song_number=0;
                                    break;
                                }
                                if(status==prevsong)
                                {
                                    status=prev_status;;
                                    if(song_number>0)
                                        song_number--;
                                    else
                                        song_number=4;
                                    break;
                                }
                                if(status==fastplay)
                                {
                                    status=prev_status;
                                    lower_status=nothing;
                                    vs_FastPlay();
                                }
                                if(status==normalplay)
                                {
                                    status=prev_status;;
                                    lower_status=nothing;
                                    vs_NormalPlay();
                                }
                                if(status==volumeup)
                                {
                                    status=prev_status;
                                    lower_status=nothing;
                                    vs_VolumeUp();
                                }
                                if(status==volumedown)
                                {
                                    status=prev_status;
                                    lower_status=nothing;
                                    vs_VolumeDown();
                                }
                                if(status==mute)
                                {
                                    status=prev_status;
                                    lower_status=nothing;
                                    vs_VolumeMute();
                                }
                                if(status==unmute)
                                {
                                    status=prev_status;
                                    lower_status=nothing;
                                    vs_VolumeUnmute();
                                }
                            }
                        }
                        if(status==playing)
                        {
                            ff = f_read(&fil, line, sizeof(line), &br);
                            if(ff==FR_OK || br >= sizeof(line))
                            {
                                vs_sdiWrite(line, sizeof(line));
                            }
                            else if(br<sizeof(line))
                                song_number++;
                        }
                    }while(ff==FR_OK || br >= sizeof(line));
                    //vs_sendZeros();
                    //vs_sendZeros();
                    ff = f_close(&fil);
                }
            }
        }
    }
    ff=f_mount(0,"",0);
    if(ff!=FR_OK)
    {
        printf("f_unmount error: %d\n", ff);
    }
    free(g_sFatFs);
    vs_softReset();
    while(1);
}
