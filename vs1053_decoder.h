/*
 * vs1063_decoder.h
 *
 *  Created on: Dec 4, 2017
 *      Author: KiranHegde
 */

#ifndef VS1053_DECODER_H_
#define VS1053_DECODER_H_

#include "msp.h"
#include <stdint.h>
#include <stdbool.h>

#define VS_WRITE_COMMAND (0x02)
#define VS_READ_COMMAND (0x03)

#define ASSERT_VSCS() (P2OUT &= ~BIT6)
#define DEASSERT_VSCS() (P2OUT |= BIT6)

#define ASSERT_VSDCS() (P2OUT &= ~BIT4)
#define DEASSERT_VSDCS() (P2OUT |= BIT4)

#define SCI_MODE            0x0
#define SCI_STATUS          0x1
#define SCI_BASS            0x2
#define SCI_CLOCKF          0x3
#define SCI_DECODE_TIME     0x4
#define SCI_AUDATA          0x5
#define SCI_WRAM            0x6
#define SCI_WRAMADDR        0x7
#define SCI_HDAT0           0x8
#define SCI_HDAT1           0x9
#define SCI_AIADDR          0xA
#define SCI_VOL             0xB
#define SCI_AICTRL0         0xC
#define SCI_AICTRL1         0xD
#define SCI_AICTRL2         0xE
#define SCI_AICTRL3         0xF


#define SM_DIFF             0x0001

#define SM_LAYER12          0x0002

#define SM_RESET            0x0004

#define SM_CANCEL           0x0008

#define SM_EARSPEAKER_LO    0x0010

#define SM_TESTS            0x0020

#define SM_STREAM           0x0040

#define SM_EARSPEAKER_HI    0x0080

#define SM_DACT             0x0100

#define SM_SDIORD           0x0200

#define SM_SDISHARE         0x0400

#define SM_SDINEW           0x0800

#define SM_ADPCM            0x1000

#define SM_PAUSE            0x2000  // note: Only availble with patch. This only quickly pauses the VS's internal buffer, when canceling quickly. It won't unpause.

#define SM_LINE1            0x4000

#define SM_CLK_RANGE 0x8000


extern char line[4096];
extern uint8_t vol_var;

enum status
{
    play,
    pause,
    stop,
    fastplay,
    playing,
    paused,
    stopped,
    fastplaying,
    normalplay,
    volumeup,
    volumedown,
    mute,
    unmute,
    volume,
    speed,
    nextsong,
    prevsong,
    nothing
}current_status;


extern uint8_t status;
extern uint8_t lower_status;
extern uint8_t mute_status;
extern uint8_t prev_status;

void vs_hardReset(void);
void vs_softReset(void);
void delay(void);
void vs_sciWrite(uint8_t reg, uint16_t value);
uint16_t vs_sciRead(uint8_t reg);
void vs_sdiWrite(uint8_t *ptr, uint32_t len);
uint16_t vs_decodeTime(void);
void vs_setVolume(uint8_t, uint8_t);
void vs_Initialization();
void vs_gpioConfigure();
bool vs_readyData();
void vs_sineTestStart();
void vs_sineTestStop();
void vs_sendZeros();
void vs_VolumeUp();
void vs_VolumeDown();
void vs_VolumeUnmute();
void vs_VolumeMute();
void vs_FastPlay();
void vs_NormalPlay();


#endif /* VS1053_DECODER_H_ */
